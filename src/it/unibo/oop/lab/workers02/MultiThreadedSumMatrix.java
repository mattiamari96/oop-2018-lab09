package it.unibo.oop.lab.workers02;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class MultiThreadedSumMatrix implements SumMatrix {

	int numThreads;
	private final ExecutorService pool;

	public MultiThreadedSumMatrix(int n) {
		this.numThreads = n;
		this.pool = Executors.newFixedThreadPool(n);
	}

	@Override
	public double sum(double[][] matrix) {
		double sum = 0;

		final List<Future<Double>> futures = Arrays.stream(matrix).map(
			row -> this.pool.submit(() -> {
				return Arrays.stream(row).sum();
			})
		).collect(Collectors.toList());

		sum = futures.stream().map(future -> {
			try {
				return future.get();
			} catch (InterruptedException | ExecutionException e) {
				throw new RuntimeException(e);
			}
		}).reduce(0.0, (a, b) -> a += b);

		return sum;
	}

}
