package it.unibo.oop.lab.reactivegui02;

import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.InvocationTargetException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class ConcurrentGUI extends JFrame {

	private static final double HEIGHT_PERCENT = 0.15;
	private static final double WIDTH_PERCENT = 0.2;

	private final JLabel lblNum;
	private final JButton btnUp;
	private final JButton btnDown;
	private final JButton btnStop;
	private final Counter counter;

	public ConcurrentGUI() {
		super();
		final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		this.setSize((int)(screenSize.getWidth() * WIDTH_PERCENT), (int)(screenSize.getHeight() * HEIGHT_PERCENT));
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		final JPanel panelMain = new JPanel();
		panelMain.setLayout(new GridBagLayout());

		final JPanel panelControls = new JPanel();
		panelControls.setLayout(new BoxLayout(panelControls, BoxLayout.X_AXIS));
		panelMain.add(panelControls);

		lblNum = new JLabel();
		lblNum.setText("0");
		panelControls.add(lblNum);

		counter = new Counter();

		btnUp = new JButton("Up");
		btnUp.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				counter.up();
			}

		});
		panelControls.add(btnUp);

		btnDown = new JButton("Down");
		btnDown.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				counter.down();
			}

		});
		panelControls.add(btnDown);

		btnStop = new JButton("Stop");
		btnStop.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				ConcurrentGUI.this.stopCounter();
			}

		});
		panelControls.add(btnStop);

		this.setContentPane(panelMain);

		ExecutorService execServ = Executors.newSingleThreadExecutor();
		execServ.submit(counter);

		this.setVisible(true);
	}

	public void stopCounter() {
		counter.stop();
		btnUp.setEnabled(false);
		btnDown.setEnabled(false);
		btnStop.setEnabled(false);
	}

	class Counter implements Runnable {
		private volatile int direction;
		private int counter;
		private boolean stop;

		public Counter() {
			this.stop = false;
			this.counter = 1;
			this.direction = 1;
		}

		@Override
		public void run() {
			while (!stop) {
				this.step();
			}
		}

		public void step() {
			try {
                SwingUtilities.invokeAndWait(new Runnable() {
                    @Override
					public void run() {
                        ConcurrentGUI.this.lblNum.setText(Integer.toString(Counter.this.getCount()));
                    }
                });

                this.counter += 1 * this.direction;
                Thread.sleep(100);
            } catch (InvocationTargetException | InterruptedException ex) {
                ex.printStackTrace();
            }
		}

		public void up() {
			this.direction = 1;
		}

		public void down() {
			this.direction = -1;
		}

		public void stop() {
			this.stop = true;
		}

		public int getCount() {
			return this.counter;
		}
	}
}
