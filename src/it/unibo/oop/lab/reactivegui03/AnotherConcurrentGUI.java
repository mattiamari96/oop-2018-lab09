package it.unibo.oop.lab.reactivegui03;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import it.unibo.oop.lab.reactivegui02.ConcurrentGUI;

public class AnotherConcurrentGUI extends ConcurrentGUI {

	public AnotherConcurrentGUI() {
		super();

		ExecutorService execServ = Executors.newSingleThreadExecutor();
		execServ.submit(new CounterStopper());
	}

	class CounterStopper implements Runnable {

		@Override
		public void run() {
			try {
				Thread.sleep(10 * 1000);
				AnotherConcurrentGUI.this.stopCounter();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}

	}

}
